Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

### [Unreleased]
[0.3.3b] 28 NOV 2021
- FIXED BUG scraper.scrape_pages scraped cleaned and inserted the same texts multiple
times with different ids on cli command scrape {prj_name} being called multiple times.
(see line 33 scraper.py ... the conditional)
- CHANGED mysql select 'all_pages' command at repo/scrapes/projects/sql_insert.yaml
(...but not at blueprint)

### [Unreleased]
[0.3.3a] 05 NOV 2021
- schema.py imports sql commands from yaml files in wbz/repo/scrapes. The
rules for the shape of the repo are set in blueprint v 0.0.3

[0.3.3] - 21 OCT 2021
MVP: full basic functionality to create scrape projects, index pages and scrapes
	their texts available from a simple(istic) CLI

[0.0.2] - DEC 25 2020
- refactor
