import pytest
import sqlite3
import yaml
import os

from wscr import cfg
#there are utilities in __init__.py
from .convenience_functions import PRJ_NAME, IDX_FNAME, TEXTS_FNAME, TMP_WEB_DIR
from .convenience_functions import MOCK_SPEC, SPEC_FNAME, TBL_CREATE_IDX_CMDS  

def prj_tear_down():
    for folder in  [cfg.PROJECTS, cfg.TEXTS]:
        for fh in os.listdir(folder):
            if fh.startswith(PRJ_NAME):
                os.remove(os.path.join(folder, fh))

    projects = os.listdir(cfg.PROJECTS)

    assert SPEC_FNAME not in projects
    assert IDX_FNAME not in projects

    projects = os.listdir(cfg.TEXTS)

    assert TEXTS_FNAME not in projects

@pytest.fixture()
def prjfix():
    prj_tear_down()
    yield
    prj_tear_down()

@pytest.fixture
def webfix():
    pass

    yield 
 
    for fh in os.listdir(TMP_WEB_DIR):
        os.remove(os.path.join(TMP_WEB_DIR, fh))

    try: os.rmdir(TMP_WEB_DIR)
    except FileNotFoundError: pass


@pytest.fixture
def create_mock_project():

    prj_tear_down()

    assert not os.path.exists(os.path.join(cfg.PROJECTS, SPEC_FNAME))
    assert not os.path.exists(os.path.join(cfg.PROJECTS, SPEC_FNAME))

    spec_pth = os.path.join( cfg.PROJECTS, SPEC_FNAME )
    index_pth = os.path.join( cfg.PROJECTS, IDX_FNAME )
 
    with open(spec_pth, 'w') as fh:  
        yaml.dump(MOCK_SPEC, fh )

    with sqlite3.connect(index_pth) as conn:
        c = conn.cursor()
        for tcc in TBL_CREATE_IDX_CMDS:
            c.execute(TBL_CREATE_IDX_CMDS[tcc])
        conn.commit()
    
    yield
    
    prj_tear_down()
    assert not os.path.exists(os.path.join(cfg.PROJECTS, SPEC_FNAME))
    assert not os.path.exists(os.path.join(cfg.PROJECTS, SPEC_FNAME))
