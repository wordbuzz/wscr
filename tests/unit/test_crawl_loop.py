import io

import os
import yaml 
import sqlite3

import mock
import pytest
from contextlib import redirect_stdout, redirect_stderr

from wscr import cfg
from wscr.app import schema
from wscr.app import crawler
from wscr.app import html_parser

#some utilities are in __init__
from wscr.tests.convenience_functions import mock_index, mock_visitor,SPEC_FNAME, mk_spec
from wscr.tests.convenience_functions import mock_response_404, MockParser, URLS, MARKUPS, STATUS

######################################################################

def test_crawl_pages_limit_type_is_bool(create_mock_project, monkeypatch):
    m = mock.Mock()

    spec = mk_spec()
    
    pc = crawler.PageCrawler(spec, m, m, m, [URLS[0]])    
    assert type(pc.limit) is bool
    assert pc.limit == False

def test_crawl_pages_limit_field_true_indexed_lt_max(create_mock_project, monkeypatch):
    
    spec = mk_spec( ('limit', True), ('max', 1) )
    
    index = mock_index()
    visitor = mock_visitor() 
    parser = MockParser(spec['rules'])
    monkeypatch.setattr(html_parser, 'HTMLParser', parser)
    pc = crawler.PageCrawler(spec, index, visitor, parser, [URLS[0] ])    
    
    #pre conditions:
    assert pc.limit == True
    assert pc.visit_count == 0
    assert len(pc.indexed) == 0
    assert pc.max == 1 #set by the spec
    
    #perform!
    crawl = pc.crawl_pages()

    #verify!
    assert pc.visit_count == 1
    assert len(pc.indexed) == 1
    #cannot get assert_called_with to work right ; (
    #assert visitor.visit.assert_called_with( URLS[0] )

    assert parser.find_url_links_called
    assert parser.get_page_title_called

def test_crawl_pages_limit_field_true_indexed_gt_max(create_mock_project, monkeypatch): 
    spec = mk_spec( ('limit', True), ('max', 2) )
    
    index = mock.Mock()
    visitor = mock_visitor() 
 
    assert not visitor.visit.called

    parser = MockParser(spec['rules'])
    monkeypatch.setattr(html_parser, 'HTMLParser', parser)

    pc = crawler.PageCrawler(spec, index, visitor, parser, [URLS[0]])    
    pc.visit_count = 2
 
    #pre condition
    assert pc.limit == True
    assert pc.max == 2
    assert len(pc.indexed) == 0

    #perform!
    crawl = pc.crawl_pages() 
    
    #verify!
    assert len(pc.indexed) == 0
    assert pc.visit_count == 2

    assert not visitor.visit.called

    assert not parser.find_url_links_called
    assert not parser.find_title_called

    assert not index.index_url_visit.called
 
    assert crawl == True

def test_crawl_pages_limit_field_false_indexed_gt_max(create_mock_project, monkeypatch):
    spec = mk_spec( ('limit', True), ('max', 2) )
    
    visitor = mock_visitor() 

    parser = MockParser(spec['rules'])
    monkeypatch.setattr(html_parser, 'HTMLParser', parser)

    index = mock.Mock()

    pc = crawler.PageCrawler(spec, index, visitor, parser, [URLS[0]])   
    pc.limit = False
    pc.indexed = ['url1', 'url2', 'url3'] 

    crawl = pc.crawl_pages()

    assert pc.max == 2

    #these should be the same mock!
    assert pc.pgvisitor.visit.called
    assert visitor.visit.called
    assert len(pc.indexed) == 4

def test_crawl_pages_limit_field_false_indexed_lt_max(create_mock_project, monkeypatch):
    spec = mk_spec()
    index = mock.Mock()
    index.do_select_all.return_value = ['url1'] 

    parser = MockParser(spec['rules'])
    monkeypatch.setattr(html_parser, 'HTMLParser', parser)

    visitor = mock_visitor() 
    monkeypatch.setattr(crawler.pagevisitor, 'PageVisitor', visitor)

    pc = crawler.PageCrawler(spec, index, visitor, parser,[URLS[0]])
    pc.limit = False

    assert len(pc.indexed) == 0
    crawl = pc.crawl_pages()  

    assert pc.visit_count == 1
    assert len(pc.indexed) == 1

    assert visitor.visit.called
    assert index.index_url_visit.called

    assert parser.find_url_links_called
    assert parser.get_page_title_called

    index.index_url_visit.assert_called_with(URLS[0], 
                                        MARKUPS[URLS[0]], 
                                        STATUS[URLS[0]], 
                                        parser.get_page_title())
 
def test_visit_page_response_is_not_200(create_mock_project, monkeypatch):
    spec = mk_spec()

    STATUS[URLS[0]] == 404
    
    index = mock.Mock()
    index.do_select_all.return_value = ['url1'] 

    visitor = mock_visitor(side_effect = mock_response_404 ) 
    monkeypatch.setattr(crawler.pagevisitor, 'PageVisitor', visitor)

    parser = MockParser(spec['rules'])
    monkeypatch.setattr(html_parser, 'HTMLParser', parser)

    pc = crawler.PageCrawler(spec, index, visitor, parser, [URLS[0]])

    assert len(pc.indexed) == 0
    assert len(pc.bounced) == 0 
 
    crawl = pc.crawl_pages()  

    assert pc.visit_count == 1
    assert len(pc.bounced) == 1 
    assert len(pc.indexed) == 0

    assert visitor.visit.called
    assert index.index_url_visit.called


#######################################################################
#crawl_listing
#######################################################################
def test_crawl_listing_raise_not_implemented_exception():
    assert False

#######################################################################
#get_search_results
#######################################################################
def test_get_search_results_raise_not_implemented_exception():
    assert False
