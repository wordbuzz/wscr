import io

import os
import yaml 
import sqlite3

import mock
import pytest
from contextlib import redirect_stdout, redirect_stderr

from wscr.app import schema
from wscr.app import crawler
from wscr.app import html_parser

#some utilities are in __init__
from wscr.tests.convenience_functions import mock_index, mock_visitor, MOCK_SPEC
from wscr.tests.convenience_functions import mock_response_404, MockParser, URLS, MARKUPS, STATUS

def test_start_(monkeypatch):
    idx = mock.Mock()
    idx.open()
    idx.do_select_all.return_value = ['url1', 'url2'] 
    monkeypatch.setattr(crawler.indexer, 'Indexer', idx)

    parser = MockParser(MOCK_SPEC['rules'])
    monkeypatch.setattr(html_parser, 'HTMLParser', parser)

    visitor = mock_visitor() 
    monkeypatch.setattr(crawler.pagevisitor, 'PageVisitor', visitor)

    pc = crawler.PageCrawler(MOCK_SPEC, idx, visitor, parser, [URLS[0]])
    pc.crawl_pages = mock.Mock()

    pc.start()

    assert len(pc.indexed) == 2

    assert not visitor.visit.called

    assert not parser.find_url_links_called
    assert not parser.find_title_called

    assert idx.open.called
    idx.open.assert_called_with('' , '' , False, 0)
 
    assert idx.do_select_all.called

    assert pc.crawl_pages.called
