import pytest 

from wscr.app import pagevisitor

pv = pagevisitor.PageVisitor()
#@pytest.mark.skip
class TestHTTPClient:
    def test_GET(self):
        response = pv.visit(url="http://httpbin.org")
        assert response.data.startswith(bytes('<!DOCTYPE', 'utf-8'))
        assert response.data.endswith(bytes('</html>', 'utf-8'))
