import os
import io
import json
import mock
import pytest
import sqlite3
import argparse

#app = TestApp(application)

from contextlib import redirect_stdout, redirect_stderr

from wscr import cfg
from wscr.app import fio
from wscr.app import schema
from wscr.app import validator

from wscr.app import main
from wscr.app import crawler, html_parser

from wscr.tests.convenience_functions import ARGS1, ARGS2, IDX_FNAME, SPEC_FNAME, TMP_WEB_DIR
from wscr.tests.convenience_functions import mk_spec, Application, create_web_resources
from wscr.tests.convenience_functions import URLS, MockParser, MockVisitor

def test_crawl_no_project_exists(prjfix):
    err = io.StringIO()
    args = main.get_parser(ARGS2)

    index_pth = os.path.join(cfg.PROJECTS, IDX_FNAME)

    msg = 'cannot crawl; index file \n%s\n does not exist'%(index_pth)
    
    with redirect_stderr(err), pytest.raises(Exception, match = msg) as xpct:
        main.run_cli(args = args)
    
    projects = os.listdir(cfg.PROJECTS)
    assert not SPEC_FNAME  in projects
    assert not IDX_FNAME in projects

#@pytest.mark.skip
def test_crawl_empty_crawl_project(prjfix):
    #create the project
    args = main.get_parser(ARGS1)
    main.run_cli(args = args)

    assert SPEC_FNAME in os.listdir(cfg.PROJECTS)
    assert IDX_FNAME in os.listdir(cfg.PROJECTS)

    mk_spec()

    args2 = main.get_parser(ARGS2)
    main.run_cli(args = args2)

    db = os.path.join(cfg.PROJECTS, IDX_FNAME)
    conn = sqlite3.connect(db)
    cursor = conn.cursor()

    sql = "SELECT * FROM URLs"

    cursor.execute(sql)

    results = cursor.fetchall()

    assert len(results) == 0
