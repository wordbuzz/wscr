import os

#messy but works
HOME = os.path.split(os.path.abspath(__file__))[0]
WBZ = os.path.split(HOME)[0]

LOG = os.path.join(HOME, 'scrapelog.log')
APP = os.path.join(HOME, 'app')

TESTS = os.path.join(HOME, 'tests')
#=======================================================================
# DATA DIRECTORIES
#=======================================================================
REPO = os.path.join(WBZ, 'repo')

CORPORA = os.path.join(REPO, 'corpora')

SCRAPES = os.path.join(REPO, 'scrapes')

PROJECTS = os.path.join(SCRAPES, 'projects')
SPEC_VAL_FNAME = 'spec_validator.yaml'
SPEC_VAL_PTH = os.path.join(PROJECTS, SPEC_VAL_FNAME)

TEXTS = os.path.join(SCRAPES, 'texts')
CLEANER_VAL_FNAME = 'cleaner_validator.yaml'
CLEANER_VAL_PTH = os.path.join(TEXTS, CLEANER_VAL_FNAME)

BOOKS = os.path.join(REPO, 'books')
