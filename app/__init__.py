import os
import sys

local = os.path.split(os.path.abspath(__file__))[0]
mother = os.path.split(local)[0]
gmother = os.path.split(mother)[0]
sys.path.append(gmother)
sys.path.append(mother)
sys.path.append(local)
