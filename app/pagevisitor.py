import certifi
import urllib3
#######################################################################
#
#######################################################################

class PVError(): pass

location_value_error = PVError()

class PageVisitor():
    def __init__(self):
        self.http = urllib3.PoolManager( cert_reqs = 'CERT_REQUIRED',
                                  ca_certs  = certifi.where() )

    def visit(self, url):
        try:
            r  = self.http.request('GET', url)

        except urllib3.exceptions.LocationValueError:
            pve = PVError()
            pve.status = 'urllib3 - [Errno ]' 
            pve.data = 'Location Value Error'
            return pve
 
        except urllib3.exceptions.MaxRetryError as e:
            pve = PVError()
            pve.status = 'urllib3 - [Errno 111]' 
            pve.data = 'Connection refused'
            return pve
            
        except:
            pve = PVError()
            pve.status = 'unknown - [Errno xyz]' 
            pve.data = 'unknown'
            return pve
 
        return r
