'''
creates repo dir  and sqlite3 db for crawl projects
'''
import os
import sys

#local = os.path.split(os.path.abspath(__file__))[0]
#mother = os.path.split(local)[0]
#sys.path.append(mother)
#sys.path.append(local)

from wscr import cfg
from wscr.app import fio
from wscr.app import dbmgr
from wscr.app import schema

msg1 = 'cannot create project specification' 
msg2 = '--> spec file \n%s\n already exists'

msg3 = 'cannot create sqlite index db for project' 
msg4 = '--> sqlite db \n%s\n already exists'

msg5 = 'cannot create cleaner guide' 
msg6 = '--> yaml file \n%s\n already exists'

msg7 = 'cannot create text repo for project' 
msg8 = '--> a directory \n%s\n already exists'

msg9 = 'There was a problem creating the project. Most likely:\n\n\n'
msg10 = '\t\t # a problem creating the project specification yaml file at %s\n'
msg11 = '\t\t # a problem creating the index sqlite db at %s\n'
msg12 = '\t\t # a problem creating the cleaner guide yaml file at %s\n'
msg13 = '\t\t #a problem creating the text repo sqlite db at %s\n'

msg14 = 'The project was created:\n\n\n'
msg15 = '\t\t # a project specification yaml file was created at %s\n'
msg16 = '\t\t # an index sqlite db was created at %s\n'
msg17 = '\t\t # a cleaner guide yaml file was created at %s\n'
msg18 = '\t\t # a text repository directory was created at %s\n'

def create_project(spec_pth, index_pth,clnr_pth, txts_pth):
    '''
    entry point
    '''
    if os.path.isfile(spec_pth):

       raise Exception(msg1 + msg2 %spec_pth)

    elif os.path.isfile(index_pth):

        raise Exception(msg3 + msg4 %index_pth)

    elif os.path.isfile(clnr_pth):

        raise Exception(msg5 + msg6 %clnr_pth)

    elif os.path.isfile(txts_pth):

        raise Exception(msg7 + msg8 %txts_pth)

    else:

        spec, idx, clnr, txts = _create_project(spec_pth, index_pth, clnr_pth, txts_pth)
        
        msg = msg9
        if not spec:

            msg += msg10 %spec_pth

        if  not idx:

            msg +=  msg11 %index_pth

        if not clnr:

            msg += msg12 %clnr_pth

        if not txts:

            msg += msg13 %txts_pth

        if spec and idx and clnr and txts:

            msg = msg14
            msg += msg15 %spec_pth
            msg += msg16 %index_pth
            msg += msg17 %clnr_pth
            msg += msg18 %txts_pth

        #print (msg)
        return msg

def _create_project(spec_pth, index_pth, clnr_pth, txts_pth):
    is_spec = _create_spec(spec_pth)
    is_index = _create_index_db(index_pth)
    is_cleaner = _create_cleaner_guide(clnr_pth)
    is_texts = _create_texts_db(txts_pth)   
    return is_spec, is_index, is_cleaner, is_texts

def _create_spec(spec_pth):
    validator = fio.read_yaml(cfg.SPEC_VAL_PTH)
    return fio.write_yaml(spec_pth, validator['model'])

def _create_cleaner_guide(clnr_pth):
    validator = fio.read_yaml(cfg.CLEANER_VAL_PTH)
    return fio.write_yaml(clnr_pth, validator['model'])

def _create_index_db(path):
    return dbmgr.create_db(path, tables = schema.CREATE_INDEX)

def _create_texts_db(path):
    return dbmgr.create_db(path, tables = schema.CREATE_TEXT_DB)
